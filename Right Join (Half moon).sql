USE Libreria

Select *
From Libro

Select *
From Persona

/* Lista de personas por nombre, apellido, genero que no tengan ningun Libro asociado por derecha*/ 
Select L.titulo, L.precio, P.nombre, P.apellido
From Libro as L
RIGHT JOIN Persona as P  on P.persona_id = L.persona_id
Where L.libro_id is null
Order BY L.titulo

/* Lista de personas agrupadas por genero que no tengan ningun libro asociado por derecha*/
Select P.nombre, P.apellido, P.genero, P.pais, P.fechaNacimiento, L.titulo
From Libro as L
RIGHT JOIN Persona as P  on P.persona_id = L.persona_id
Where L.libro_id is null